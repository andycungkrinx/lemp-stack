## LEMP Stack
```sh
Dockerize LEMP base on alpine
```

## Feature
```sh
- Traefik 2.4
- Nginx 1.20.0 alpine (include pagespeed, brotli and modsec module)
- PHP alpine
```

## How to use
```sh
- update .env for configuration (dont forget change your /etc/hosts for apply domain)
- ./run.sh (start all container)
- ./stop.sh (stop all container)
```

## Database
```sh
- You can using root user for full privilage
- Host is 127.0.0.1
- mysqldump -uroot -p[yourpassword] -h127.0.0.1 [your_database] > your_database.sql

